#include "Matrix.h"

Matrix::Matrix(int n, int m, double** matrix) {//����������� � ����������� 
	this->n = n;
	this->m = m;
	this->matrix = new double*[n];
	for (int i = 0; i < m; ++i) {
		this->matrix[i] = new double[m];
		for (int j = 0; j < m; ++j)
			this->matrix[i][j] = matrix[i][j];
	}
}
Matrix::Matrix()//����������� �� ���������
{
	n = 0;
	m = 0;
	matrix = nullptr;
}
	


int Matrix::GetN() const {//������
	return n;
}
int Matrix::GetM() const {//������
	return m;
}
//�������� ������� � ��������� �������
double*& Matrix::operator[] (int i) const
 {
	return matrix[i];
	}
Matrix Matrix::operator+(const Matrix & matrix_B) {//�������� +
	Matrix matrix_A(*this);
	if (matrix_A.GetM() != matrix_B.GetM() || matrix_A.GetN() != matrix_B.GetN()) {
		cout << "������ �������! ������� ������ �� ���������!\n";
		return Matrix();
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] += matrix_B[i][j];
	}
	return matrix_A;
}
Matrix Matrix::operator-(const Matrix & matrix_B) {//�������� -
	Matrix matrix_A(*this);
	if (matrix_A.GetM() != matrix_B.GetM() || matrix_A.GetN() != matrix_B.GetN()) {
		cout << "������ �������! ������� ������ �� ���������!\n";
		return Matrix();
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] -= matrix_B[i][j];
	}
	return matrix_A;
}
bool Matrix::operator==(const Matrix & matrix_B) const {//�������� ����������
	const Matrix& matrix_A = *this;
	if (matrix_A.GetN() != matrix_B.GetN() || matrix_A.GetM() != matrix_B.GetM())
		return false;
	for (int i = 0; i < matrix_A.GetN(); ++i) {
		for (int j = 0; j < matrix_A.GetM(); ++j) {
			if (matrix_A[i][j] != matrix_B[i][j])
				return false;
		}
	}
	return true;
}

bool Matrix::operator!=(const Matrix & matrix_B) const {//�������� �����������
	const Matrix & matrix_A = *this;
	return !(matrix_A == matrix_B);
}
void Matrix::Randomize(int n, int m) {//�������������� ���������
	this->n = n;
	this->m = m;
	matrix = new double*[n];
	for (int i = 0; i < n; ++i)
		matrix[i] = new double[m];
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix[i][j] = rand() % 10;
	}
}

Matrix::~Matrix() {//����������

	for (int i = 0; i < n; ++i)
		delete matrix[i];
	delete[] matrix;
}
//�������� ����� �� ������
istream& operator >> (istream& stream, Matrix& matrix) {
	int n;
	int m;
	stream >> n >> m;
	double** sqrt_array = new double*[n];
	for (int i = 0; i < n; ++i) {
		sqrt_array[i] = new double[m];
		for (int j = 0; j < m; ++j)
			stream >> sqrt_array[i][j];
	}
	matrix = *(new Matrix(n, m, sqrt_array));
	return stream;
}
//�������� ������ �� ������
ostream& operator << (ostream& stream, const Matrix& matrix) {
	stream << matrix.GetN() << " " << matrix.GetM() << "\n";
	for (int i = 0; i < matrix.GetN(); ++i) {
		for (int j = 0; j < matrix.GetM(); ++j)
			stream << matrix[i][j] << " ";
		stream << "\n";
	}
	return stream;
}