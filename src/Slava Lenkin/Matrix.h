#pragma once
#pragma once
#include <iostream>
#include <fstream>
using namespace std;
class Matrix
{
	//���� �����
	friend istream& operator >> (istream& stream, Matrix& matrix);
	friend ostream& operator << (ostream& stream, const Matrix& matrix);
private:
	int n;
	int m;
	double** matrix;

public:
	Matrix(int n, int m, double** matrix);//����������� � �����������
	Matrix();//����������� �� ���������
	
	int GetN() const;//������
	int GetM() const;//������
	double*& operator[] (int i) const;
	Matrix operator + (const Matrix& matrix);//�������� +
	Matrix operator - (const Matrix& matrix);//�������� -
	bool operator == (const Matrix& matrix_B) const;//�������� ����������
	bool operator != (const Matrix& matrix_B) const;//�������� �����������
	void Randomize(int n, int m);//�������������� ���������
	~Matrix();//����������
};

